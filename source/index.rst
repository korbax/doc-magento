.. MagecomDoc documentation master file, created by
   sphinx-quickstart on Thu Dec 15 23:41:03 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to MagecomDoc's documentation!
======================================

.. toctree::
    :maxdepth: 2
    :caption: Contents:

    test.rst



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
